package Colecoes;

import java.util.List;

public class Product {
	
	public double UNKNOWN_PRICE = -1;
	public double UNDEFINED_PRICE = -2;
	String productID;
	String title;
	Double price;
	List<Review> reviews;
	
	public Product(String productID, String title, Double price){
		this.productID = productID;
		this.title = title;
		this.price = price;		// o prre�o pode ser unknow ou undefined isso deve ser previsto
	}
	
	public void setReview(Review review){
		this.reviews.add(review);
	}
	
}
