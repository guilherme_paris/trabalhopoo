package Colecoes;

public class Review {

	private double score;
	private long time;
	private String summary;
	private String text;
	Helpfulness helpfulness;
	
	private User user;
	private Product product;

		
	public Review(double score, long time, String summary, String text, 
						String helpfulness, User user, Product product){
			this.score = score;
			this.time = time;
			this.summary = summary;
			this.text = text;
			this.helpfulness = new Helpfulness(helpfulness);
			this.user = user;
			this.product = product;
			
			this.user.setReview(this);
			this.product.setReview(this);
	}
	
	public void setUser(User u){
		this.user = u;
	}
	
	public void setProduct(Product p){
		this.product = p;
	}

	public double getScore() {
		return score;
	}

	public long getTime() {
		return time;
	}

	public String getSummary() {
		return summary;
	}

	public String getText() {
		return text;
	}

	public Helpfulness getHelpfulness() {
		return helpfulness;
	}

	public User getUser() {
		return user;
	}

	public Product getProduct() {
		return product;
	}
	
	
}
