package Colecoes;

import java.util.List;

public class User {
		
	String userId;
	String profileName;
	List<Review> reviews;
	
	public User(String userId, String profileName, Review review){
		this.userId = userId;
		this.profileName = profileName;
	}
	
	public void setReview(Review review){
		this.reviews.add(review);
	}
	
}
