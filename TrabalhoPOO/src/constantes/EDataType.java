package constantes;

public enum EDataType {
	PRODUCTID("product/productId: "), TITLE("product/title: "), PRICE("product/price: "),
	USERID("review/userId: "), NAME("review/profileName: "),
	HELPFULNESS("review/helpfulness: "), SCORE("review/score: "),
		TIME("review/time: "), SUMMARY("review/summary: "), TEXT("review/text: ");
	
	private final String valor;
	
	EDataType(String valorOpcao){
		valor = valorOpcao;
	}
	
	public String getValor(){
		return valor;
	}
}
