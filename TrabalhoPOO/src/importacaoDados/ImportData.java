package importacaoDados;

import java.io.IOException;
import java.util.Scanner;
import constantes.EDataType;
import Colecoes.DataManager;

public class ImportData {
	
	public ImportData(Scanner scan, DataManager data) throws IOException{
		
		String line  = null;
		scan.useDelimiter("\n");
		while (scan.hasNext()) {
		    
			System.out.println("Linha: "+line);
		    
		    String productID = scan.next().replace(EDataType.PRODUCTID.getValor(), "");
		    String title = scan.next().replace(EDataType.TITLE.getValor(), "");
		    String userID = scan.next().replace(EDataType.USERID.getValor(), "");
		    String profileName = scan.next().replace(EDataType.PRODUCTID.getValor(), "");
		    String summary = scan.next().replace(EDataType.SUMMARY.getValor(), "");
		    String text = scan.next().replace(EDataType.TEXT.getValor(), "");
		    
		    Double price = Double.parseDouble(scan.next().replace(EDataType.PRICE.getValor(), ""));
		    Double helpfulness = Double.parseDouble(scan.next().replace(EDataType.HELPFULNESS.getValor(), ""));
		    Double score = Double.parseDouble(scan.next().replace(EDataType.SCORE.getValor(), ""));
		    Double time = Double.parseDouble(scan.next().replace(EDataType.TIME.getValor(), ""));
		    
			data.incluir(productID, title, price, userID, profileName, 
					score, time, summary, text);
		    
		    
		}
		
	
	}
	
	
}
